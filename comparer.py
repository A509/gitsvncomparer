#%% Standard library imports
# You have to call this from directory of the git repository (not the .git folder, but the folder containing the .git folder).
# git clean doesn't seem to work very well with --git-dir, it might even completely ignore it. Some kind of bug?

import os, sys

#%% Modifiable constants
# warning: I'm assuming these constants don't have any spaces in them
cGitWorkingDirectory = 'eduke32_git'
cSVNDirectory = '../eduke32_svn'
cGitDirectory = '.'

# collected using:
# git log --sparse --pretty="%h %B __END75__" > _fullcommitmessage.txt
# very hokey but it works
cRevFilePath = '../_fullcommitmessage.txt'

cMaxRevisionNumber = 8634
# only differs from cMaxRevisionNumber for testing purposes
cIterationLimit = 5

#%% Functions
def CommitsFromLines(fileString):
	cDelimiter = '__END75__'
	noNewLines = fileString.replace('\n', '')

	commitRawEntries = noNewLines.split(cDelimiter)
	
	dataDictionary = dict()
	
	for commit in commitRawEntries:
		if(commit == ''):
			continue
		
		spaceIndex = commit.find(' ')
		commitHash = commit[0:spaceIndex]
		message = commit[spaceIndex+1:]
		rString = 'from_svn_r'
		revSectionPosition = commit.find(rString)
		revIndexPosition = revSectionPosition + len(rString) 
		revString = commit[revIndexPosition :]
		revInt = int(revString)
	
		entry = [revInt, commitHash, message]
		
		if(revInt in dataDictionary):
			print(f'Unexpected error: duplicate rev number {revInt}')
			return None
			
		dataDictionary[revInt] = entry
	
	return dataDictionary

#%% Script main

startDirectory = os.getcwd()
print(f'script started in {startDirectory}')
os.chdir(cGitWorkingDirectory)

print(f'Changed working directory to {os.getcwd()} (should be git directory)')
#sys.exit(1)

# I think I have to ignore .gitignore because from the very early commits in git there's a .gitignore, even though it doesn't exist
# for a long time in svn
diffCommand = f'diff {cSVNDirectory} {cGitDirectory} --exclude .svn --exclude .git --exclude .gitignore'

# this command is just to make sure the git version and svn version didn't go out of sync somehow
gitLogCommand = f'git log -1'

gitResetCommand = f'git reset --hard'

with open(cRevFilePath) as revFile:
	fileString = revFile.read()

commitDict = CommitsFromLines(fileString)

# for some reason, it looks like svn r2 is the same as the first commit in git
for key in sorted(commitDict):
	commitData = commitDict[key]
	(svnRevision, gitHash, message) = commitData

	gitRevCommand = f'git checkout {gitHash}'

	svnCommand = f'svn update {cSVNDirectory} -r{svnRevision}'

	print(f'Using commands [{gitRevCommand}, {svnCommand}, {diffCommand}, {gitLogCommand}]')

	print(gitResetCommand)
	os.system(gitResetCommand)

	print(gitCleanCommand)
	os.system(gitCleanCommand)

	print(gitRevCommand)
	gitRevRetVal = os.system(gitRevCommand)

	if(gitRevRetVal != 0):
		print(f'git rev switch command failed with error {gitRevRetVal}')
		sys.exit(1)

	print(gitLogCommand)
	os.system(gitLogCommand)

	print(gitResetCommand)
	os.system(gitResetCommand)

	# 	for some reason, the git clean directory doesn't seem to be necessary if I'm using
	# 	the git repository as the current working directory for all the git commands

	print(svnCommand)
	svnRetVal = os.system(svnCommand)

	if(svnRetVal != 0):
		print(f'svn command failed with error {svnRetVal}')
		sys.exit(1)

	print('Diff results below:')
	print(diffCommand)
	diffRetVal = os.system(diffCommand)
	
	if(diffRetVal != 0):
		print(f'Diff command gave retval {diffRetVal}')
		sys.exit(1)
